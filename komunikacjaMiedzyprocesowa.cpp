// wwektory proj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <stdio.h>
#include <vector>
#include <list>

using namespace std;

const string stany[5] = { "nowy", "wykonywany", "oczekiwanie", "gotowy", "zakonczony" };
class Proces;
vector<Proces> procesy;
class Semafor{
public:

	struct Type
	{
		enum{ TYPE_COUNTER = 0, TYPE_BOOL = 1 };
		int id;
		Type(int id)
		{
			this->id = id;
		}
		Type(){};
	};

	Type type;
	int counter;
	bool occupied;
	Semafor(Type type)
	{
		this->type = type;
		this->counter = false;
		this->counter = 0;
	}
	Semafor(){};
	int getCounter()
	{
		return counter;
	}

	bool increment()
	{
		if (type.id == Type::TYPE_BOOL)
		{
			if (this->occupied)
			{
				return false;
			}
			occupied = true;
			return true;
		}
		if (counter < 0)
		{
			counter = 0;
		}
		counter += 1;
		return true;
	}


	void decrement()
	{
		if (type.id == Type::TYPE_BOOL)
		{
			occupied = false;
		}
		if (counter < 1){
			cout << "*****************************************************************************************" <<
				"Probujesz zmniejszyc semafor o wartosci 0. Proces jest zablokowany i czeka na nowa wiadomosc\n"
				<< "*****************************************************************************************\n";
			counter = -1;
			return;
		}
		counter -= 1;
	}

	bool isOccupied()
	{
		return occupied;
	}
	bool block()
	{
		if (this->type.id == Type::TYPE_BOOL)
		{
			cout << "___== Proces jest zablokowany i czeka na wiadomosc. ==____\n\n";
			return false;
		}
		setOccupied(true);
		return true;
	}
	
	void setOccupied(bool occupied)
	{
		this->occupied = occupied;
	}
};

class msg
{
private:
	string tresc;


public:
	Proces *nadawca;
	int idnad;
	msg(Proces *nadawca, int idnad, string tresc) :nadawca(nadawca), idnad(idnad), tresc(tresc){};
	msg(){};

	string gettext()
	{
		return tresc;
	}

};


class Proces{
private:

	list <msg> wiadomosci;


public:
	Semafor MESSAGE_SEMAPHORE_RECEIVER;
	Semafor MESSAGE_SEMAPHORE_COMMON;

	int id;
	string stan;
	msg pierwsza;
	void pierwszamsg()
	{
		this->pierwsza = this->wiadomosci.front();
	}


	int getid()
	{
		return this->id;
	}


	bool wyslij(Proces *proces, int idnad, string tresc){
		// zabrane z if proces->stan == stany[2] ||
		if (proces->stan == stany[4])
		{
			cout << "||||   Proces do ktorego chcesz wyslac wiadomosc jest zakonczony.   ||||\n" << endl;
			return false;
		}

		proces->stan = stany[1];
		if (proces->stan == stany[1])
		{
			//proces.wiadomosci.push_back(msg(proces.id, tresc));
			proces->wiadomosci.push_back(msg(proces, this->id, tresc));
			proces->MESSAGE_SEMAPHORE_RECEIVER.increment();

			proces->stan = stany[3];
			this->stan = stany[1];
			

			return true;
		}
		
		

	}
	//void odbierz(Proces *proces)
	void odbierz()
	{
		wake();
		if (MESSAGE_SEMAPHORE_RECEIVER.getCounter() > 0){

			pierwszamsg();
			cout << "Proces nr: " << this->id << "  Wiadomosc: " << this->pierwsza.gettext() << "  Nadawca: " << pierwsza.idnad << endl << endl;
			this->wiadomosci.pop_front();
			this->stan = stany[1];

		}
		MESSAGE_SEMAPHORE_COMMON.setOccupied(false);

		MESSAGE_SEMAPHORE_RECEIVER.decrement();
		if (MESSAGE_SEMAPHORE_RECEIVER.counter < 0)
		{
			this->stan = stany[2];
		}
		

		
	}

	
	Proces(int id);
	Proces(){};
	//Proces(const Proces &proces);
	bool wake()
	{
		if (stan == stany[2])
		{
			cout << "Proces jest blokowany\n" << endl;
			return false;
		}
		this->stan = stany[1];
		return true;
	}


	void wyswietl(int id)
	{
		cout << "Proces nr: " << Proces::id << "   Stan: " << stan << "  Liczba wiadomosci: " << MESSAGE_SEMAPHORE_RECEIVER.getCounter() << endl << endl;
	}


};
Proces::Proces(int id) :id(id){
	stan = stany[0];
	this->MESSAGE_SEMAPHORE_COMMON = Semafor(Semafor::Type::TYPE_BOOL);
	this->MESSAGE_SEMAPHORE_RECEIVER = Semafor(Semafor::Type::TYPE_COUNTER);


};
/*Proces::Proces(const Proces &proces)
{
stan = stan[0];
}*/





int _tmain(int argc, _TCHAR* argv[])
{
	
	int l;
	int x;
	do{
		cout << "  ||  1) Dodaj proces.                ||\n" <<
			"  ||  2) Wyswietl wszystkie procesy.  ||\n" <<
			"  ||  3) Wybierz proces do dzialania. ||\n" <<
			"  ||  4) Zakoncz program.             ||" << endl;
		cin >> l;
		switch (l){
			//PIERWSZE MENU WYBORU!!!!!
		case 1:
			procesy.push_back(Proces(procesy.size()));
			break;
		case 2:
			if (procesy.empty())
			{
				cout << "Brak procesow do wyswietlenia!\n\n";
			}
			else{
				for (int i = 0; i < procesy.size(); i++)
				{
					procesy[i].wyswietl(i);
				}
			}
			break;
		case 3:

			if (procesy.empty())
			{
				cout << "\nBrak procesow do dzialania!\n\n";
				break;
			}
			cout << "Wybierasz proces numer: ";

			cin >> x;

			if (x>(procesy.size() - 1))
			{
				cout << "=======================\n"
					<< "Nie ma takiego procesu!" << endl
					<< "=======================\n\n";
				break;
			}
			if (procesy[x].stan == stany[2] && procesy[x].MESSAGE_SEMAPHORE_RECEIVER.counter < 0)
			{
				cout << "\n\nNie mozna dzialac na procesie poniewaz\njest on zablokowany i czeka na wiadomosc\n\n";
				break;
			}
			if (procesy[x].stan == stany[4])
			{
				cout << "\n\nNie mozna dzialac na procesie\nponiewaz jest on zakonczony\n\n";
				break;
			}
			cout << "\nWybrales proces numer: " << x << endl;
			procesy[x].stan = stany[3];
			cout << "  ||  1) Wyslij wiadomosc.      ||\n" <<
				"  ||  2)Odbierz wiadomosc.      ||\n" <<
				"  ||  3)Zakoncz proces.         ||\n";

			int y;
			cin >> y;
			string tekst;
			int nrprocesu;
			switch (y){//PODMENU 3) WYBIERZ PROCES DO DZIALANIA
			case 1:
				cout << "Wpisz proces do ktorego chcesz wyslac wiadomosc: ";
				cin >> nrprocesu;
				if (nrprocesu == x)
				{
					cout << "===================================================\n"
						<< "Proces nie moze wyslac wiadomosci do samego siebie!" << endl
						<< "===================================================\n\n";
					break;
				}
				else if (nrprocesu > (procesy.size() - 1))
				{
					cout << "=======================\n"
						<< "Nie ma takiego procesu!" << endl
						<< "=======================\n\n";
					break;
				}
				cout << "\nWpisz tresc wiadomosci: ";
				cin >> tekst;
				procesy[x].wyslij(&procesy[nrprocesu], procesy[x].id, tekst);
				/////////////////////////////////////////////////////////////
				for (int z = 0; z < procesy.size(); z++)
				{
					if (procesy[z].id != procesy[x].id)
					{
						if (procesy[z].stan == stany[1])
						{
							procesy[z].stan = stany[3];
						}
					}
				}
				break;
			case 2:
				/*cout << "Wpisz proces z ktorego chcesz odczytac wiadomosc: ";
				cin >> nrprocesu;
				if (nrprocesu != x)
				{
				cout << "Wybrales zly proces do odczytu.\n\n";
				break;
				}
				else
				{*/
				cout << endl << endl;
				procesy[x].odbierz();

				for (int z = 0; z < procesy.size(); z++)
				{
					if (procesy[z].id != procesy[x].id)
					{
						if (procesy[z].stan == stany[1])
						{
							procesy[z].stan = stany[3];
						}
					}
				}


				break;
			case 3:
				procesy[x].stan = stany[4];
				break;

			default:
				cout << "Nieprawidlowa komenda!\n\n";
				break;
			}

			break;
			 

		}

	} while (l != 4);

	return 0;
}

